<?php
defined('_JEXEC') or die();
require __DIR__.'/helper.php';

$lng = (JFactory::getLanguage()->getTag() == 'it-IT') ? 'it' : 'en';

$doc = JFactory::getDocument();
$doc->addStyleSheet(JUri::base()."modules/mod_iit_domains/css/iitdomains.css");

$label = ($lng == 'it') ? "comprimi" :"collapse domains";
$label_expand = $lng == "it" ? "vedi tutti i domini" :"see all domains";

$doc->addScriptDeclaration('
    $(document).ready(function() {
    $("#domains-label").click(function(e) {
    if ( $("#domains").is(".domains")) {
        $("#domains").removeClass("domains");
        $("#domains-label .fa").removeClass("fa-angle-down");
        $("#domains-label .fa").addClass("fa-angle-up");
         $(".str-to-upper").html("'.$label.'");
    }
    else {
        $("#domains").addClass("domains");
        $("#domains-label .fa").removeClass("fa-angle-up");
        $("#domains-label .fa").addClass("fa-angle-down");
        $(".str-to-upper").html("'.$label_expand .'");
    }
    e.preventDefault();
  });
});
');

$domains = modIitdomains::getdomains();


$name = 'name_'.$lng;
$alias = 'alias_'.$lng;
$description = 'description_'.$lng;


require JModuleHelper::getLayoutPath('mod_iit_domains','default'); 