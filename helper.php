<?php
defined('_JEXEC') or die();

abstract class modIitDomains
{
    public static function  getDomains()
    {
        $config = JFactory::getConfig();
        $apiBaseUrl = $config->get('api_host');
        $http = new JHttp();
        $apiResponse = $http->get($apiBaseUrl.'/relation/domains');
        if ($apiResponse->code == 200){
            $rawData = json_decode($apiResponse->body);
            $domains = $rawData->_embedded->domain;
            $selected = array();
            $pCount = count($domains);
            
            while (count($selected) < 4)
            {
                $rand = rand(0, $pCount-1);
                if (!in_array($rand , $selected)) {                                     
                    $tmp = $domains[count($selected)];
                    $domains[count($selected)] = $domains[$rand];
                    $domains[$rand] = $tmp;
                    
                    $selected[] = $rand;
                }
            }
            return $domains;
        }
        
        return false;
        
    }
    
    public static function buildDomainUlrFromAlias($alias) 
    {
        $artId = 0;
        $catId = 0;
        $itemId = 0;
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $query->select('id,catid')
              ->from($db->quoteName('#__content'))
              ->where($db->quoteName('alias') . ' = '. $db->quote($alias));
        $db->setQuery($query);

        $results = $db->loadObject();
        if ($results){
            $artId = $results->id;
            $catId = $results->catid;
        }
        else {
            return '#';
        }
        $query = $db->getQuery(true);
        $query->select('id')
              ->from($db->quoteName('#__menu'))
              ->where($db->quoteName('alias').' = '.$db->quote($alias))
              ;
        $db->setQuery($query);
        $results = $db->loadObject();
        if ($results){
            $itemId = $results->id;
        }
        else {
            return '#';
        }
        
        return JRoute::_("index.php?option=com_content&view=article&id=$artId&catid=$catId&Itemid=$itemId");
            
    }
}