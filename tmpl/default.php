<?php
defined('_JEXEC') or die();

?>
<h2 class="iit-title"><?php echo $lng == 'it' ? 'Domainmi di ricerca' : 'Domains'?>
    <a	href="<?php echo modIitDomains::buildDomainUlrFromAlias('domains')?>"
		title="<?php echo $lng == 'it' ? 'Tutti i domini': 'All domains'?>">
		<span class="fa fa-angle-right pull-right"></span>
	</a>
</h2>
<section id="domains" class="domains">
    <div class="row">
        <?php foreach ($domains as $index => $domain):?>
        <?php $link = modIitDomains::buildDomainUlrFromAlias($domain->{$alias})?>
            <div class="col-xs-8 col-xs-offset-2 col-sm-6 col-sm-offset-0 col-md-3 col-md-offset-0">
    			<div class="iit_prg_media_type">
    			 <a href="<?php echo $link?>">
    				<img class="mod-prg-img" alt=""
    					src="<?php echo $domain->img_source?>" />
    			 </a>
    			</div>
    			<a	href="<?php echo $link?>">
    			   <h4	class="" style="font-weight: bold;"><?php echo $domain->{$name} ;?></h4>
    			</a>
    			<a href="<?php echo $link?>"><p class="prg-description"><?php echo $domain->{$description}?></p></a>
    		</div>
        <?php endforeach;?>
        <div style="clear: both;"></div>
    </div>
</section>
<!--
<div class="row">
	<h5 id="domains-label"
		class="col-xs-10 col-xs-offset-1 col-sm-4 col-sm-offset-4 col-md-4 col-md-offset-4 all-domains-label">
		<span class="str-to-upper"><?php echo $lng == 'it' ? 'vedi tutti i domini' : 'see all domains' ?></span>
		<span class="fa fa-angle-down pull-right"></span>
	</h5>
</div>
-->